package com.tsystems.javaschool.tasks.subsequence;

import javax.xml.transform.Transformer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
/**
 * Created by alexpench on 22.02.17.
 */
public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean isPossible = true;

        if(x == null || y == null){
            throw new IllegalArgumentException();
        }

        if (x.size() > y.size()) {
            return false;
        }

        if (x.size() == y.size()) {
            return x.equals(y);
        }

        for (int i = 0; i < x.size()&&isPossible; i++) {
            int indexFrom = y.indexOf(x.get(i));
            int lengthY = y.size();
                if( indexFrom != -1){
                    y = y.subList(++indexFrom,lengthY);
                } else {isPossible = false;}
        }

        return isPossible;
    }
}
