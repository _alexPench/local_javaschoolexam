package com.tsystems.javaschool.tasks.duplicates;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.*;
/**
 * Created by alexpench on 22.02.17.
 */
public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by NUMBER of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */

    public boolean process(File sourceFile, File targetFile) {
        boolean isConverted = false;
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }
        SortedMap<String, Integer> map = new TreeMap<>();
        try (Scanner input = new Scanner(sourceFile)) {
            findDuplicates(map, input);
            writeToFile(map, targetFile);
            isConverted = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return isConverted;
    }

    void findDuplicates(SortedMap<String, Integer> map, Scanner input) {
        while (input.hasNext()) {
            String line = input.next();
            if (map.get(line) != null) {
                Integer val = map.get(line);
                val++;
                map.put(line, val);
            } else {
                map.put(line, 1);
            }
        }
    }

    private void writeToFile(Map<String, Integer> map, File targetFile) {
        try (FileWriter fstream = new FileWriter(targetFile, true)) {
            BufferedWriter out = new BufferedWriter(fstream);
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                out.write(String.format("%s[%d]%n", entry.getKey(), entry.getValue()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}