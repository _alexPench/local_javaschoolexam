package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by alexpench on 22.02.17.
 */
public enum Symbols {
    OPEN_SC, CLOSE_SC, NUMBER, PLUS, MINUS, MULT, DIVISION, DOT;
}
