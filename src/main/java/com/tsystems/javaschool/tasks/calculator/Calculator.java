package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.NoSuchElementException;
/**
 * Created by alexpench on 22.02.17.
 */
public class Calculator {

    private int index = 0;

    public String evaluate(String statement) {
        String result = null;

        if (statement != null && !statement.contains(",")) {

            DecimalFormat df = new DecimalFormat("#.####");
            ArrayList<SymbolValueMap> parsedStatement;
            try {
                parsedStatement = parse(statement);
                result = df.format(plusMinus(parsedStatement)).replace(",", ".");
            } catch (Exception e) {
                return null;
            }
            if (result.charAt(0) == 8734) {
                result = null;
            }
        }
        return result;
    }

    private ArrayList<SymbolValueMap> parse(String expression) throws Exception {

        ArrayList<SymbolValueMap> result = new ArrayList<>(0);
        int i = 0;
        int eLength = expression.length();

        while (i < eLength) {
            while (i < eLength && Character.isSpaceChar(expression.charAt(i))) {
                ++i;
            }
            switch (expression.charAt(i)) {
                case '+':
                    result.add(new SymbolValueMap(Symbols.PLUS, 0));
                    ++i;
                    continue;
                case '-':
                    result.add(new SymbolValueMap(Symbols.MINUS, 0));
                    ++i;
                    continue;
                case '*':
                    result.add(new SymbolValueMap(Symbols.MULT, 0));
                    ++i;
                    continue;
                case '/':
                    result.add(new SymbolValueMap(Symbols.DIVISION, 0));
                    ++i;
                    continue;
                case '(':
                    result.add(new SymbolValueMap(Symbols.OPEN_SC, 0));
                    ++i;
                    continue;
                case ')':
                    result.add(new SymbolValueMap(Symbols.CLOSE_SC, 0));
                    ++i;
                    continue;
            }

            if (Character.isDigit(expression.charAt(i))) {
                String resultVal = "";
                while (i < eLength && (Character.isDigit(expression.charAt(i)) || expression.charAt(i) == '.')) {
                    resultVal += expression.charAt(i);
                    ++i;
                }
                result.add(new SymbolValueMap(Symbols.NUMBER, Float.parseFloat(resultVal)));
                continue;
            }
        }
        return result;
    }

    private float numSC(ArrayList<SymbolValueMap> sMap) throws Exception {
        SymbolValueMap current = sMap.get(index);
        float result = 0;

        switch (current.symbols) {
            case NUMBER:
                result = current.value;
                ++index;
                break;

            case OPEN_SC:
                ++index;
                result = plusMinus(sMap);

                if (sMap.get(index).symbols.equals(Symbols.CLOSE_SC)) {
                    ++index;
                } else {
                    throw new NoSuchElementException();
                }
                break;

            default:
                throw new NoSuchElementException();
        }
        return result;
    }

    private float plusMinus(ArrayList<SymbolValueMap> sample) throws Exception {
        int sSize = sample.size();
        float result = muldOrDev(sample);
        while (index < sSize && (sample.get(index).symbols.equals(Symbols.PLUS) || sample.get(index).symbols.equals(Symbols.MINUS))) {
            if (sample.get(index).symbols.equals(Symbols.PLUS)) {
                ++index;
                result += muldOrDev(sample);
            } else {
                ++index;
                result -= muldOrDev(sample);
            }
        }

        return result;

    }

    private float muldOrDev(ArrayList<SymbolValueMap> sample) throws Exception {
        int n = sample.size();
        float result = numSC(sample);

        while (index < n && (sample.get(index).symbols.equals(Symbols.MULT) || sample.get(index).symbols.equals(Symbols.DIVISION))) {

            if (sample.get(index).symbols.equals(Symbols.MULT)) {
                ++index;
                result *= numSC(sample);
            } else {
                if (sample.get(index).symbols.equals(Symbols.DIVISION)) {
                    ++index;
                    result /= numSC(sample);
                }
            }

        }

        return result;
    }
}

