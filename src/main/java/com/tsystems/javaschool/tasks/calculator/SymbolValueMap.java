package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by alexpench on 22.02.17.
 */
public class SymbolValueMap {
    Symbols symbols;
    float value;
    public SymbolValueMap(Symbols symbols, float value){
        this.symbols = symbols;
        this.value = value;
    }
}
