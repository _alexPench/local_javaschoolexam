package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        duplicateFinder.process(null, new File("b.txt"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        duplicateFinder.process(new File("a.txt"), null);
    }

    @Test
    public void test2(){
        SortedMap<String, Integer> map = new TreeMap<>();
        String input = "aaa bbb ccc aaaa ccc dddd yyyyy";
        Scanner s = new Scanner(input).useDelimiter("\\s");
        duplicateFinder.findDuplicates(map, s);
        Map<String, Integer> expectedResult = new TreeMap<>();
        expectedResult.put("aaa", 1);
        expectedResult.put("aaaa", 1);
        expectedResult.put("bbb", 1);
        expectedResult.put("ccc", 2);
        expectedResult.put("dddd", 1);
        expectedResult.put("yyyyy", 1);
        Assert.assertEquals(expectedResult, map);
    }

    @Test
    public void test3(){
        SortedMap<String, Integer> map = new TreeMap<>();
        String input = "kk b b ttt";
        Scanner s = new Scanner(input).useDelimiter("\\s");
        duplicateFinder.findDuplicates(map, s);
        Map<String, Integer> expectedResult = new TreeMap<>();
        expectedResult.put("ttt", 1);
        expectedResult.put("kk", 1);
        expectedResult.put("b", 1);
        Assert.assertNotEquals(expectedResult, map);
    }
}